package com.example.demo.Test.netty.example.simple;

import com.example.demo.Test.netty.base.consts.Constants;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LineBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestNettyClient {
    private SocketChannel socketChannel;

    public ChannelFuture connect(String host, int port) throws InterruptedException {

        EventLoopGroup group = new NioEventLoopGroup();
        try {
            Bootstrap b = new Bootstrap();
            b.group(group)
                    .channel(NioSocketChannel.class)
                    .option(ChannelOption.SO_KEEPALIVE, true)
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            ChannelPipeline p = ch.pipeline();
                            p.addLast(new LineBasedFrameDecoder(1024));
                            p.addLast(new StringDecoder());
                            p.addLast(new StringEncoder());
                            p.addLast(new LineClientHandler());
                        }
                    });

            ChannelFuture future = b.connect(Constants.HOST, Constants.PORT).sync();
            if (future.isSuccess()) {
                socketChannel = (SocketChannel) future.channel();
                System.out.println("connect simpecs  成功");
            }
            return future;


        } finally {
            // group.shutdownGracefully();
        }
    }


    public static void main(String[] args) {
        try {
            ChannelFuture future = new TestNettyClient().connect(Constants.HOST, Constants.PORT);
            String msg = "a long msg";
            for (int i = 0; i < 100; i++) {
                msg += " big bigest " + i;
            }
            future.channel().writeAndFlush(msg + System.getProperty("line.separator"));
//            future.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


}


class LineClientHandler extends ChannelInboundHandlerAdapter {
    private Logger logger = LoggerFactory.getLogger(getClass());

    private int count = 0;

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        // Send the message to Server
        for (int i = 0; i < 1; i++) {

            String body = "Hello world from client:" + i + System.getProperty("line.separator");

            ctx.channel().writeAndFlush(body);
        }
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg)
            throws Exception {
        String body = (String) msg;
        count++;
        logger.info("client read msg:{}, count:{}", body, count);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
            throws Exception {
        logger.error("client caught exception", cause);
        ctx.close();
    }
}
