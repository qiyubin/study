package com.example.demo.Test.netty.example.ed;

import com.example.demo.Test.netty.base.consts.Constants;
import com.example.demo.Test.netty.base.endecode.Message;
import com.example.demo.Test.netty.base.init.PipeLineTools;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestNettyClient {
    private SocketChannel socketChannel;

    public ChannelFuture connect(String host, int port) throws InterruptedException {

        EventLoopGroup group = new NioEventLoopGroup();
        try {
            Bootstrap b = new Bootstrap();
            b.group(group)
                    .channel(NioSocketChannel.class)
                    .option(ChannelOption.SO_KEEPALIVE, true)
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            ChannelPipeline p = ch.pipeline();
                            PipeLineTools.initEncodeAndDecode(p);
                            p.addLast(new ClientMessageHandler());
                        }
                    });

            ChannelFuture future = b.connect(Constants.HOST, Constants.PORT).sync();
            if (future.isSuccess()) {
                socketChannel = (SocketChannel) future.channel();
                System.out.println("connect simpecs  成功");
            }
            return future;


        } finally {
            // group.shutdownGracefully();
        }
    }


    public static void main(String[] args) {
        try {
            ChannelFuture future = new TestNettyClient().connect(Constants.HOST, Constants.PORT);
            String msg = "a long msg";
            for (int i = 0; i < 100; i++) {
                msg += " big bigest " + i;
            }

            Message msgs = new Message(msg, (short) 0, (byte) 0xAF);

            future.channel().writeAndFlush(msgs);
//            future.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


}


class ClientMessageHandler extends SimpleChannelInboundHandler<Message> {
    private Logger logger = LoggerFactory.getLogger(getClass());

    private int count = 0;


    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, Message message) throws Exception {
        count++;
        logger.info("client read msg:{}, count:{}", message.getData(), count);

    }
}
