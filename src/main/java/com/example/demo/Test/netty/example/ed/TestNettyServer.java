package com.example.demo.Test.netty.example.ed;

import com.example.demo.Test.netty.base.consts.Constants;
import com.example.demo.Test.netty.base.endecode.Message;
import com.example.demo.Test.netty.base.init.PipeLineTools;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestNettyServer {

    private Logger logger = LoggerFactory.getLogger(getClass());


    public void bind(int port) throws Exception {
        EventLoopGroup bossGroup = new NioEventLoopGroup(1);
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        ServerBootstrap b = new ServerBootstrap();
        b.group(bossGroup, workerGroup)
                .channel(NioServerSocketChannel.class).option(ChannelOption.SO_BACKLOG, 1024)
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    public void initChannel(SocketChannel ch) throws Exception {

                        ChannelPipeline p = ch.pipeline();
                        PipeLineTools.initEncodeAndDecode(p);


                        p.addLast(new ServerMessageHandler());
                    }
                });
        // Bind and start to accept incoming connections.
        ChannelFuture f = b.bind(port).sync(); // (7)
        logger.info("simpecs bind port:{}", port);
        // Wait until the simpecs socket is closed.
        f.channel().closeFuture().sync();
    }

    public static void main(String[] args) {
        try {
            new TestNettyServer().bind(Constants.PORT);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

}


class ServerMessageHandler extends SimpleChannelInboundHandler<Message> {
    private Logger logger = LoggerFactory.getLogger(getClass());

    private int count = 0;


    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Message message) throws Exception {
        count++;

        logger.info("server read msg:{}, count:{}", message.getData(), count);

        Message msgs = new Message("ok", (short) 0, (byte) 0xAF);
        ctx.writeAndFlush(msgs);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
            throws Exception {
        logger.error("simpecs caught exception", cause);
        ctx.close();
    }


}
