package com.example.demo.Test.netty.base.init;

import com.example.demo.Test.netty.base.endecode.MessageDecoder;
import com.example.demo.Test.netty.base.endecode.MessageEncoder;
import io.netty.channel.ChannelPipeline;

public class PipeLineTools {

    //TODO 参考Message
    //body(4)+zip(1)+cmdId(2)+type(1)=8
    //最大长度
    private static final int MAX_FRAME_LENGTH = 1024 * 1024;
    //这个值就是MessageEncoder body.length(4)
    private static final int LENGTH_FIELD_LENGTH = 4;
    //这个值就是MessageEncoder zip(1)+cmdId(2)+type(1)
    private static final int LENGTH_FIELD_OFFSET = 4;
    private static final int LENGTH_ADJUSTMENT = 0;
    private static final int INITIAL_BYTES_TO_STRIP = 0;

    public static ChannelPipeline initEncodeAndDecode(ChannelPipeline p) {

        p.addLast(new MessageDecoder(MAX_FRAME_LENGTH, LENGTH_FIELD_LENGTH, LENGTH_FIELD_OFFSET, LENGTH_ADJUSTMENT, INITIAL_BYTES_TO_STRIP));
        p.addLast(new MessageEncoder());
        return p;
    }


}
