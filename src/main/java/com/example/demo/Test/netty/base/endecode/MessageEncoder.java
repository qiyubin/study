package com.example.demo.Test.netty.base.endecode;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

import java.nio.charset.Charset;

public class MessageEncoder extends MessageToByteEncoder<Message> {

	// 编码格式
	private final Charset charset = Charset.forName("UTF-8");
	// 需要压缩的长度
	private final int compressLength = 1024;

	@Override
	protected void encode(ChannelHandlerContext ctx, Message msg, ByteBuf out) throws Exception {
		String source = msg.getData();
		byte[] body = source.getBytes(charset);
//		if (body.length > compressLength) {
//			msg.setZip((byte) 1);
//			// 加压
//			body = ZipTool.compress(body);
//		}

		//cmdId(2)+type(1)+zip(1)+body(4)=8
		//out = Unpooled.directBuffer(8+body.length);

		//cmdId
		out.writeShort(msg.getCmdId());
		//type
		out.writeByte(msg.getType());
		//是否加压
		out.writeByte(msg.getZip());
		//长度
		out.writeInt(body.length);
		//内容
		out.writeBytes(body);
	}

}