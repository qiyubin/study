package com.example.demo.Test.netty.base.endecode;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;

import java.nio.charset.Charset;

public class MessageDecoder extends LengthFieldBasedFrameDecoder {

    //body(4)+zip(1)+cmdId(2)+type(1)=8
    private static final int HEADER_SIZE = 8;

    // 编码格式
    private final Charset charset = Charset.forName("UTF-8");

    public MessageDecoder(int maxFrameLength, int lengthFieldOffset, int lengthFieldLength) {
        super(maxFrameLength, lengthFieldOffset, lengthFieldLength);
    }

    public MessageDecoder(int maxFrameLength, int lengthFieldOffset, int lengthFieldLength, int lengthAdjustment, int initialBytesToStrip) {
        super(maxFrameLength, lengthFieldOffset, lengthFieldLength, lengthAdjustment, initialBytesToStrip);
    }

    @Override
    protected Object decode(ChannelHandlerContext ctx, ByteBuf in) throws Exception {

        // 消息头读取不完整，不做解析返回null，直到读完整为止
        if (in.readableBytes() <= HEADER_SIZE) {
            return null;
        }

        in.markReaderIndex();

        short cmdId = in.readShort();
        byte type = in.readByte();
        byte zip = in.readByte();
        int dataLength = in.readInt();

        // TODO 网络信号不好，没有接收到完整数据
        if (in.readableBytes() < dataLength) {
            //保存当前读到的数据，下一次继续读取
            //断包处理：查看ByteToMessageDecoder的channelRead方法，ByteBuf cumulation属性
            in.resetReaderIndex();
            return null;
        }

        byte[] data = new byte[dataLength];
        in.readBytes(data);

        // TODO 手动释放内存
        //in.release(); // or ReferenceCountUtil.release(in);

        //判断是否压缩

        String body = new String(data, charset);
        Message msg = new Message(body, cmdId, type);
        return msg;
    }
}