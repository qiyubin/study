package com.example.demo.Test.netty.example.simple;

import com.example.demo.Test.netty.base.consts.Constants;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.LineBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestNettyServer {

    private Logger logger = LoggerFactory.getLogger(getClass());


    public void bind(int port) throws Exception {
        EventLoopGroup bossGroup = new NioEventLoopGroup(1);
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        ServerBootstrap b = new ServerBootstrap();
        b.group(bossGroup, workerGroup)
                .channel(NioServerSocketChannel.class).option(ChannelOption.SO_BACKLOG, 1024)
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    public void initChannel(SocketChannel ch) throws Exception {

                        ChannelPipeline p = ch.pipeline();
                        p.addLast(new LineBasedFrameDecoder(1024));
                        p.addLast(new StringDecoder());
                        p.addLast(new StringEncoder());

                        p.addLast(new LineServerHandler());
                    }
                });
        // Bind and start to accept incoming connections.
        ChannelFuture f = b.bind(port).sync(); // (7)
        logger.info("simpecs bind port:{}", port);
        // Wait until the simpecs socket is closed.
        f.channel().closeFuture().sync();
    }

    public static void main(String[] args) {
        try {
            new TestNettyServer().bind(Constants.PORT);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

}


class LineServerHandler extends ChannelInboundHandlerAdapter {
    private Logger logger = LoggerFactory.getLogger(getClass());

    private int count = 0;

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg)
            throws Exception {

        count++;
        String body = (String) msg;
        logger.info("simpecs read msg:{}, count:{}", body, count);

        String response = "hello from simpecs" + System.getProperty("line.separator");
        ctx.writeAndFlush(response);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
            throws Exception {
        logger.error("simpecs caught exception", cause);
        ctx.close();
    }


}
