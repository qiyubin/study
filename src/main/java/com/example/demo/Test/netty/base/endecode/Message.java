package com.example.demo.Test.netty.base.endecode;

public class Message {
	
	/**
	 * 要发送的数据
	 */
	private String data;
	
	/**
	 * 业务编号
	 */
	private short cmdId;
	
	/**
	 * 消息类型  0xAF 表示心跳包    0xBF 表示超时包  0xCF 业务信息包
	 */
	private byte type;
	
	/**
	 * 是否压缩，1是，0不是
	 */
	private	byte zip = 0 ;
	
    /**
     * 封装要发送的数据包
     * @param data 业务数据
     * @param cmdId 业务标识号
     * @param type 消息类型
     */
	public Message(String data,short cmdId,byte type){
		this.data=data;
		this.cmdId=cmdId;
		this.type=type;
	}
 
	public String getData() {
		return data;
	}
 
	public void setData(String data) {
		this.data = data;
	}
 
	public short getCmdId() {
		return cmdId;
	}
 
	public void setCmdId(short cmdId) {
		this.cmdId = cmdId;
	}
 
	public byte getType() {
		return type;
	}
 
	public void setType(byte type) {
		this.type = type;
	}
 
	public byte getZip() {
		return zip;
	}
 
	public void setZip(byte zip) {
		this.zip = zip;
	}
}