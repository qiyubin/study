package com.example.demo.Test;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Created by qiyubin on 2019/2/13 0013.
 *
 * @author qiyubin
 */
public class WriteReadLockTest {

    static ReentrantReadWriteLock reentrantReadWriteLock = new ReentrantReadWriteLock();


    final static CountDownLatch latch = new CountDownLatch(1);


    public static void main(String[] args) throws BrokenBarrierException, InterruptedException {
//        CyclicBarrier cyclicBarrier = new CyclicBarrier(1, new Runnable() {
//            @Override
//            public void run() {
//                reentrantReadWriteLock.readLock().unlock();
//
//            }
//        });

        reentrantReadWriteLock.writeLock().lock();
        reentrantReadWriteLock.readLock().lock();
        reentrantReadWriteLock.readLock().lock();
        System.out.println("finish");
        reentrantReadWriteLock.writeLock().unlock();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    reentrantReadWriteLock.readLock().lock();
                    reentrantReadWriteLock.readLock().unlock();
                } catch (Exception e) {

                }
                latch.countDown();

            }
        });
        thread.start();
        latch.await();
        reentrantReadWriteLock.readLock().unlock();
        reentrantReadWriteLock.readLock().unlock();
        reentrantReadWriteLock.readLock().unlock();

    }

}
