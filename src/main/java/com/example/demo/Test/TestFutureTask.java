package com.example.demo.Test;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.Semaphore;

/**
 * Created by qiyubin on 2019/2/11 0011.
 *
 * @author qiyubin
 */
public class TestFutureTask {


    public static void manySeeOne(int n, FutureTask futureTask) {
        Semaphore semaphore = new Semaphore(2);
        for (int i = 0; i < n; i++) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        semaphore.acquire();
                        futureTask.get();
                        System.out.println("i see");
                        semaphore.release();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                }
            });
            thread.start();
        }
    }

    public static void main(String[] args) {

        FutureTask futureTask = new FutureTask(
                new Callable() {
                    @Override
                    public Object call() throws Exception {
                        Thread thread = Thread.currentThread();

                        thread.sleep(1000);
                        return null;
                    }
                }
        );
        Thread thread = new Thread(futureTask);
        thread.start();
        manySeeOne(5, futureTask);
//        try {
//            futureTask.get();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        } catch (ExecutionException e) {
//            e.printStackTrace();
//        }
    }


}
