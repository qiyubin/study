package com.example.demo.Test;

import com.example.demo.dao.entity.Balance;

import javax.xml.ws.BindingType;
import java.util.Random;

/**
 * Created by qiyubin on 2019/2/12 0012.
 *
 * @author qiyubin
 */
public class ClassLoaderTest {


    public static void main(String[] args) {
        System.out.println("test");
//        Aa.anInt = 2;
        //静态常量不会引起类加载
//        System.out.println(Aa.b);
        //静态变量赋值、使用都会引起类加载
//        System.out.println(Aa.a);

//        System.out.println(Aa.bb2);
//            Aa aa=new Aa();

        while (true) {
            try {
                Thread.currentThread().sleep(1000l);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }


}

final class Cc {
    static {
        System.out.println("Cc load");
    }
}

class Bb {
    static {
        System.out.println("Bb load");
    }
}

class Aa {

    final static int b = 2;
    final static Bb bb2 = new Bb();
    static Bb bb = new Bb();
    static int a = 1;
    static int ra = (int) Math.random() * 10;


    static int doIt() {
        return 2;
    }

    static {
        System.out.println("Aa load " + bb2);
    }

}

