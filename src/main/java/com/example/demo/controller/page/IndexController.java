package com.example.demo.controller.page;

import com.example.demo.dao.entity.Balance;
import com.example.demo.dao.entity.BalanceExample;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by qiyubin on 2019/2/11 0011.
 *
 * @author qiyubin
 */
@Controller
@RequestMapping(value = "/")
public class IndexController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public Object index() {
        return "index";
    }
}
