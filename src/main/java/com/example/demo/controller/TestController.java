package com.example.demo.controller;

import com.example.demo.dao.entity.Balance;
import com.example.demo.dao.entity.BalanceExample;
import com.example.demo.dao.mapper.BalanceMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by qiyubin on 2019/2/10 0010.
 *
 * @author qiyubin
 */
@Controller
@RequestMapping(value = "/api/test", produces = "application/json;charset=UTF-8")
public class TestController {

    @Resource
    BalanceMapper balanceMapper;

    @RequestMapping(value = "/select", method = RequestMethod.GET)
    @ResponseBody
    public Object select() {
        List<Balance> balances = balanceMapper.selectByExample(new BalanceExample());
        return balances;
    }


    @RequestMapping(value = "/{uid}", method = RequestMethod.GET)
    @ResponseBody
    public Object test(@PathVariable String uid) {

        return "{\"A\":\"1\"}";
    }

}
